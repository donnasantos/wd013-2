
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/style/style.css">
    <title>process_birthstone</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/slate/bootstrap.css">
</head>
<body>
    
 
<?php


    $username = $_POST['username'];
    $birthmonth = $_POST['birthmonth'];
     
    
   

 function getBirthStone(){
    $username = $_POST['username'];
    $birthmonth = $_POST['birthmonth'];
    
    if($username === "" ||  $birthmonth === ""){
        echo "Please input a username and a valid birthmonth";
    };
    if ($birthmonth === "January"){
        echo "Garnet";
    };
    if ($birthmonth === "February"){
        echo "Amethyst";
    };
    if ($birthmonth === "March"){
        echo "Bloodstone/Aquamarine";
    };
    if ($birthmonth === "April"){
        echo "Diamond";
    };
    if ($birthmonth === "May"){
        echo "Emerald";
    };
    if ($birthmonth === "June"){
        echo "Pearl/Alexandrite";
    };
    if ($birthmonth === "July"){
        echo "Ruby";
    };
    if ($birthmonth === "August"){
        echo "Sardonyx/Peridot";
    };
    if ($birthmonth === "September"){
        echo "Sapphire";
    };
    if ($birthmonth === "October"){
        echo "Opal";
    };
    if ($birthmonth === "November"){
        echo "Turqoise" ;
    }
    if ($birthmonth === "December"){
        echo "Topaz/Citrine";
    };
    }

    function getPersonality(){
        $username = $_POST['username'];
        $birthmonth = $_POST['birthmonth'];
        
        if($username === "" ||  $birthmonth === ""){
            echo "No Data. Verify your input.";
        };
        if($birthmonth !== "January" && $birthmonth !== "February" && $birthmonth !== "March" && $birthmonth !== "April" && $birthmonth !== "May" && $birthmonth !== "June" && $birthmonth !== "July" && $birthmonth !== "August" && $birthmonth !== "September" && $birthmonth !== "October" && $birthmonth !== "Npvember" && $birthmonth !== "December"){
            echo "Invalid. Kindly Check your month. Example, if January, type January.";
        };
        if ($birthmonth === "January"){
            echo "Garnet represents confidence, energy and willpower. Those born in this month tend to be feisty and spontaneous, willing to take on any challenge that comes their way. Whether you're a lover or a fighter, this gem will keep you protected and warn of impending danger.";
        };
        if ($birthmonth === "February"){
            echo "Those with an amethyst birthstone are known for being highly intelligent with excellent business sense. They often have a pure mind full of positive and optimistic thoughts. With a simple word or calming presence, they can set others at ease. They are also known for being great listeners, able to lend an ear to a friend in need.";
        };
        if ($birthmonth === "March"){
            echo "Those with an bloodstone birthstone can invoke the tranquility of the sea. Like those born in the previous month, March babies can summon a sense of calm in the people around them. They're known as great communicators, deeply valued for their compassion and honesty. Bloodstone personalities make for great mediators, adept at settling disputes in the fairest way possible. They weigh both sides of an argument and consider all parties involved. Even under pressure, they're able to think clearly, express their thoughts and make decisions";
        };
        if ($birthmonth === "April"){
            echo "Someone born in April is a person you can depend on. While fearless and stubborn, they're also fiercely loyal, willing to stand up for friends and family in times of need. A diamond is a grandma who takes in stray family members without a home and volunteers at the soup kitchen. Or an overseas volunteer building schools for underprivileged children. You might think a diamond personality would be cocky, but those born in April are known for their innocence. They often possess a strong character and strict moral code, valuing truth and trust above all else. Due to their sparkly nature, they also have a fondness for all things luxury";
        };
        if ($birthmonth === "May"){
            echo "Emerald's luxurious green color reflects new growth. Those born in this month tend to be loving and compassionate. They have a knack for helping people see past their differences and find common ground. Often, friends seek you out for advice on work, family issues or relationships. The gemstone has links to love and fertility. Those born in May tend to be deep-thinking romantics, seeking out flirtatious encounters and lifelong soulmates. Once you are friends with a May personality, prepare to have them in your life forever. They seek truth over falseness and value honest relationships.";
        };
        if ($birthmonth === "June"){
            echo "You might hear people call alexandrite “emerald by day, ruby by night” due to its unique color-changing abilities. Those with this birthstone tend to have a keen eye, keeping watch on everything and everyone around them. If an alexandrite personality spots a problem, they're already working on a solution. Roman historian Pliny the Elder gave moonstone its name, claiming it shifted appearance with the moon's phases. The gem comes in a variety of shades, from yellow, peach and pink to green and gray. Those born in June often have keen instincts and should listen to their intuition. They always seem to make decisions at the most opportune moments.";
        };
        if ($birthmonth === "July"){
            echo "It's no surprise those with a ruby personality are often showy and prone to drama. They love to sing and take part in theater performances. Their self-confidence makes them excellent leaders, even at a young age.       Those born in July are also known for their love and passion — in all aspects of life, not just relationships. They radiate energy and dazzle everyone around them. Instead of sitting around waiting for the adventure, those born in this month are prone to action. They want to be at the front of the line leading the crowd";
        };
        if ($birthmonth === "August"){
            echo "Those born in this month have a reputation for being incredibly kind. They offer a welcoming light to anyone they see, both strangers and friends. They tend to be open with others because they are unafraid to be themselves. Others tend to trust peridot personalities instantly due to their friendly and welcoming nature. Those born in August tend to be highly extroverted and effortlessly charming. They don't mind the spotlight or being the center of attention. While they don't seek drama, they're able to take control of a crowd and show conviction when facing a challenge.";
        };
        if ($birthmonth === "September"){
            echo "Sapphire birthstone bearers tend to have a quiet type of dignity signaling intelligence. They have a keen sense of calm in any situation and, as a result, can make calculated decisions in high-pressure environments.    While those born in September can think on their feet, you won't find them bragging about it. You're more likely to hear about accomplishments secondhand from a friend or family member. They're incredibly humble and would rather talk about others over themselves.";
        };
        if ($birthmonth === "October"){
            echo "Opal birthstone holders are supposedly protective and faithful, loyal to those close to them. They tend to inspire positivity everywhere they go, making them both good leaders and effective mentors. They're the ones volunteering and taking part in community projects. They're concerned about the good of all, not just themselves.

            ";
        };
        if ($birthmonth === "Those born in November are also known for being the life of the party. They love to have a good time and, as a result, people gravitate toward them. They're often the center of their friend group or the glue that holds the family together. Friends often describe them as generous, with an incredible capacity to forgive past transgressions."){
            echo "Topaz";
        }
        if ($birthmonth === "December"){
            echo "December-born are also turquoise birthstone holders. The mineral, ranging in color from sky blue to yellow-green, was popular among ancient Aztecs and Egyptians. King Tut's golden funeral mask was even inlaid with turquoise. Those with a turquoise personality are known to be honest and wise. They also know how to persevere. Once they have a goal, they go on to achieve it.";
        };
        }
?>

        <h1>Hello <?php echo $username; ?></h1>
        <h2 id="birthMonthResult">Your birthmonth is <?php echo $birthmonth; ?></h2>
        <h2 id="birthstone">Birthstone: <?php echo getBirthStone(); ?></h2>
        <div id="stonediv" class="stone d-flex">
             <img id="stone" src="../assets/images/stone.png" alt="">
        <h3 id="personality">Personality: <br><br><?php echo getPersonality(); ?></h3>
        </div>
 



</body>
</html>
