<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://bootswatch.com/4/slate/bootstrap.css">
    <link rel="stylesheet" href="../assets/style/style.css">
    <title>Birthstone</title>
</head>
<body>
    <div class="d-flex justify-content-center align-items-center flex-column vh-100">
    <img id="stone1" src="assets/images/stone1.png" alt="">
    <img id="stone2" src="assets/images/stone2.png" alt="">
    <img id="stone3" src="assets/images/stone3.png" alt="">
    <img id="stone4" src="assets/images/stone4.png" alt="">
    <img id="stone5" src="assets/images/stone5.png" alt="">
    <img id="stone6" src="assets/images/stone6.png" alt="">
    
        <h2> Let us know your birthstone</h2>
    <form action="controllers/process_birthstone.php" method="POST">
        <div class="form-group">
            <label for="username">Username: </label>
            <input type="text" name="username" class="form-control">
        </div>
            <label for="username">Birthmonth: </label>
            <input type="text" name="birthmonth" class="form-control">       
        <button type="submit" class="btn btn-success m-2">Submit</button>

      
    
    </form>
</div>
    
</body>
</html>