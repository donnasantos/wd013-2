<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://bootswatch.com/4/solar/bootstrap.css">
    <title>Login Page</title>
</head>
<body>
    <div class="d-flex justify-content-center align-items-center flex-column vh-100">
        <h1>Login Page</h1>
        <!-- METHOD = POST -->
    <form action="controllers/process_login.php" method="POST">
        <div class="form-group">
            <label for="email">Email: </label>
            <!-- INPUTS MUST HAVE NAMES -->
            <input type="email" name="email" class="form-control">
        </div>
            <input type="password" name="password" class="form-control">
        <!-- BUTTON INSIDE THE FORM MUST BE TYPE SUBMIT -->
        <button type="submit" class="btn btn-info">Submit</button>
    
    </form>
</div>
    
</body>
</html>